//This is a program to calculate the sum of positive integers
//until a zero or negative integer is entered
#include <stdio.h>
int main (){
	int number, sum = 0;

	//Entering a value by the user
	printf ("Please enter an integer: \n", number);
	
	//Reading the entered value
	scanf ("%d \n", &number);

	while (number > 0) {
	
	sum += number;
	scanf ("%d", &number);
	}

	//Printing the total sum 
	printf ("The total sum is: %d \n", sum);

	return 0;
}

