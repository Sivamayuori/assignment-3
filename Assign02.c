//This is a program to identify if a number is prime number or not
#include <stdio.h>
int main (){
	int num, i, j=0;

	//Entering a number by the user
	printf ("Please enter a number: \n");

	//Reading the entered number
	scanf ("%d", &num);

	//Checking if the number is prime or not
	for (i=1; i<=num; i++){
		if ((num%i)==0){
			j++;
		}
	}
	if (j==2)
		printf ("%d is a prime number \n", num);
	else
		printf ("%d is not a prime number \n", num);

	return 0;
}
