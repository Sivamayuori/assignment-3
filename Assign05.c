//This is a program to print multiplication tables from 1 to n
#include <stdio.h>
int main (){
	int number=0, i, j;

	//Entering a number by the user
	printf ("Please enter a number: ");

	//Reading the entered value
	scanf ("%d", &number);

	//Generating multiplication table
	for (i=1; i<=number; i++){
		for (j=1; j<=10; j++){
			if (j<=number-1)
			printf ("%d*%d = %d \n",j,i,i*j);

			else
				printf ("%d*%d = %d \n", j,i,i*j);
		}
		printf ("\n");
	}

	return 0;
}
