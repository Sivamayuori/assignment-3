//This is a program to reverse a number
#include <stdio.h>
int main(){
	int number, reverseNum=0, remainder;

	//Entering a number by the user
	printf ("Please enter a number: \n");

	//Reading the entered value
	scanf ("%d", &number);
	
	//Reversing the number entered
	while (number !=0){
		remainder = number%10;
		reverseNum = reverseNum*10+remainder;
		number /=10;
	}

	printf ("Reversed number is: %d \n",reverseNum);

	return 0;
}
