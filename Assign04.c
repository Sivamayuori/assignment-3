//This is a program to find factors of a number
#include <stdio.h>
int main (){
	int num,i;

	//Entering a number by the user
	printf ("Please enter a number: \n");

	//Reading the entered number
	scanf ("%d", &num);

	printf ("All factors of %d are: ", num);

	for (i=1; i<=num; i++){
		if (num % i==0){
			printf ("%d ", i);
		}
	}
	printf ("\n");

	return 0;
}


